<?php
namespace User\Model;


use DVGroup\Db\Model\BaseTable;
use Everyman\Neo4j\Cypher\Query;

class TbUser extends BaseTable
{

  public static function tableName()
  {
    return 'tb_user';
  }

  public function attributes()
  {
    return [
      'id', 'username', 'fullname', 'authen_key', 'password', 'status', 'avatar_path', 'avatar',
      'gender', 'birthday', 'adress', 'description', 'email', 'point', 'group', 'created_date'
    ];
  }

  /**
   * thuc hien dang ky user
   * @param $data
   * @return \Exception|int
   * @throws \Exception
   */
  public function register($data)
  {
    //$k=new \Zend\Math\Rand();
    $data['auth_key'] = \Zend\Math\Rand::getString(40);
    $info             = array(
      'username'    => $data['username'],
      'password'    => $data['password'],
      'fullname'    => $data['fullname'],
      'status'      => 10,
      'birthday'    => date('Y-m-d H:i:s', strtotime($data['birthday'])),
      'address'     => $data['adress'],
      'email'       => $data['email'],
      'auth_key'    => $data['auth_key'],
      'group'       => 0,
      'create_date' => date("Y-m-d H:i:s")
    );
    if ($this->getUserByUsername($data['username'])) {
      return 0;
    }
    try {
      $model = $this->tableGateway->insert($info);
      $id    = $this->tableGateway->getLastInsertValue();
      $redis = $this->createRedis($data);
      if ($id) {
        $neo4j = $this->createPersonNode($id, $data['username']);
        if (!$neo4j || !$redis) {//xoa du lieu neu neu khong co redis va neo
          if ($neo4j) {
            $this->deleteNodeNeo4j($id, $data['username']);
          }
          if ($redis) {
            $this->deleteUserRedis($data['username']);
          }
          $this->deleteUserSql($data['username']);
        }
      }
    } catch (\Exception $e) {
      throw $e;
    }

  }

  /**
   * @param $username
   * @return array|\ArrayObject|bool|null
   */
  public function getUserByUsername($username)
  {
    $rowset = $this->tableGateway->select(array('username' => $username));
    $row    = $rowset->current();
    if (!$row) return false;
    return $row;
  }

  /**
   * thuc thi login user
   * @param $data
   * @return int
   * @throws \Zend\Crypt\Password\InvalidParamException
   */
  public function login($data)
  {
    $row = $this->getUserByUsername($data['username']);
    if (!$row) return 0;
    $encrypt = \Zend\Crypt\Password\Bcrypt::validatePassword($data['password'], $row['password']);
    if (!$encrypt) return 0;
    else {
      $sessionManager = new \Zend\Session\SessionManager();
      $ttl            = $data['remember'] ? 60 * 60 * 24 * 30 * 12 : 60 * 60 * 24 * 30;
      $sessionManager->rememberMe($ttl);
      //$sessionManager->setConfig($sessionConfig);
      $session = new \Zend\Session\Container('USER');
      $session->setExpirationSeconds($ttl);
      $session->user = serialize($row);
      $session->username = $row['username'];
      $session->group    = $row['group'];
      return 1;
    }
  }

  /**
   * tao user redis khi dang ky
   * @param $data
   * @return bool
   * @throws \Exception
   */
  public function createRedis($data)
  {
    $redis = $this->getRedis();
    $table = $this->tableName();
    $key   = $table . ':profile:' . $data['username'];
    try {
      $redis->rPush($table, $data['username']);
      foreach ($this->attributes() as $value) {
        if (isset($data[$value])) {
          $redis->hSet($key, $value, $data[$value]);
        } else {
          $redis->hSet($key, $value, '');
        }
      }
      return TRUE;
    } catch (\Exception $e) {
      return FALSE;/*throw $e;*/
    }
  }

  /**
   * tao node person trong neo4j khi nguoi dung dang ky tk
   * @param $id
   * @param $username
   * @return bool|\Everyman\Neo4j\Query\ResultSet
   */
  public function createPersonNode($id, $username)
  {
    if ($this->checkExitedUsername($username)) {
      return false;
    }
    $format = 'CREATE (n:Person {username: "%s", id: "%d"}) RETURN n';
    $insertQuery = sprintf($format, $username, $id);
    $client = $this->getNeo4j();
    $query = new Query($client, $insertQuery);
    return $query->getResultSet() ? 1 : 0;
  }

  /**
   * kiem tra node person da duoc toa trong neo4j chua
   * @param $username
   * @return bool
   */
  public function checkExitedUsername ($username) {
    $format = 'MATCH (n:Person {username: "%s"}) RETURN n';
    $insertQuery = sprintf($format, $username);
    $client = $this->getNeo4j();
    $query = new Query($client, $insertQuery);
    $check = $query->getResultSet();
    if ($check && isset($check[0])) {
      return true;
    } else
      return false;
  }

  /**
   * xoa node trong truong hop ko co du lieu tren db
   * @param $id
   * @param $username
   * @param string $table
   * @return \Everyman\Neo4j\Query\ResultSet
   */
  public function deleteNodeNeo4j($id, $username, $table = 'Person')
  {
    $format = 'MATCH (n:"%s" {id: "%s", username: "%s"}) DELETE n';
    $insertQuery = sprintf($format, $table, $id, $username);
    $client = $this->getNeo4j();
    $query = new Query($client, $insertQuery);
    return $query->getResultSet();
  }

  /**
   * xoa user co username tuong ung
   * @param $username
   * @return int
   */
  public function deleteUserSql($username){
    return $this->tableGateway->delete(['username' => $username]);
  }

  /**
   * xoa user redis vua tao trong truong hop khong day du db neo4j
   * @param $username
   */
  public function deleteUserRedis($username)
  {
    $redis = $this->getRedis();
    $table = $this->tableName() ;
    $redis->del($table. ':profile:' . $username);
    $redis->rPop($table);
  }

}