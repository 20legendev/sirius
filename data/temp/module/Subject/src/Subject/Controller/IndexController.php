<?php

    namespace Subject\Controller;

    use Zend\Mvc\Controller\AbstractActionController;
    use Zend\View\Model\ViewModel;
    use Zend\Predis\Client;
    use Zend\Paginator\Paginator,
        Zend\Paginator\Adapter\Null as PageNull;
    use Zend\ModuleManager;


    class IndexController extends AbstractActionController
    {
        const PAGE     = 1;
        const PER_PAGE_HOT = 15; // số bản ghi trên 1 trang, chủ đề hot
        const PER_PAGE_USER = 7; // số bản ghi trên 1 trang, chủ đề dành cho bạn
        const PAGE_RANGE = 5; //số phân trang

        public function indexAction()
        {

            $view        = new ViewModel;
            $subjectHot  = $this->forward()->dispatch('Subject\Controller\Widget', array('action' => 'subject-process', 'page' => self::PAGE, 'perPage' => self::PER_PAGE_HOT, 'pageRange' => self::PAGE_RANGE, 'type' => 'hot','template' => 'subject-hot'));
            $subjectUser = $this->forward()->dispatch('Subject\Controller\Widget', array('action' => 'subject-process', 'page' => self::PAGE, 'perPage' =>self::PER_PAGE_USER, 'pageRange' => self::PAGE_RANGE, 'type' => 'user','template' => 'subject-user'));
            $view->addChild($subjectHot, 'subjectHot');
            $view->addChild($subjectUser, 'subjectUser');
            return $view;
        }
    }
