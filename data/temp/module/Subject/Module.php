<?php
    /**
     * Zend Framework (http://framework.zend.com/)
     * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
     * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
     * @license   http://framework.zend.com/license/new-bsd New BSD License
     */

    namespace Subject;

    use Subject\Model\Subject;
    use Subject\Model\SubjectMusic;
    use Subject\Model\SubjectMusicTable;
    use Subject\Model\SubjectTable;
    use Zend\Session;
    use Zend\Db\ResultSet\ResultSet;
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Mvc\ModuleRouteListener;
    use Zend\Mvc\MvcEvent;

    class Module
    {

        public function getConfig()
        {
            return include __DIR__ . '/config/module.config.php';
        }

        public function getAutoloaderConfig()
        {
            return array(
                'Zend\Loader\StandardAutoloader' => array(
                    'namespaces' => array(
                        __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    ),
                ),
            );
        }

        public function getServiceConfig()
        {
            return array(
                'initializers' => array(
                    function ($instance, $sm) {
                        if ($instance instanceof \Zend\Db\Adapter\AdapterAwareInterface) {
                            $instance->setDbAdapter($sm->get('Zend\Db\Adapter\Adapter'));
                        }
                    }
                ),
                'factories'    => array(
                    'SubjectTable'      => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            return new SubjectTable(new TableGateway('tb_subject', $dbAdapter));
                        },
                    'SubjectMusicTable' => function ($sm) {
                            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                            return new SubjectMusicTable(new TableGateway('tb_subject_music', $dbAdapter));
                        },

                ),
            );
        }
    }
