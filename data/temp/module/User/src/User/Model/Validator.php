<?php
namespace User\Model;
use Zend\Session\Container;
Class Validator
{
    protected $session;
    public function __construct()
    {
        $this->session=new Container("USER");
    }
    
    public function IsValid()
    { 
        if($this->session->offsetExists('username'))
        {
            return true;
        }
        return false;
    }
    public function IsUser($username)
    {              
        if($this->session->username!==$username)
        {
            return true;
        }
        return false;
    }
    public  function getGroup()
    {
        return $this->session->group;
    }
    public function getUser()
    {
        return $this->session->username;
    }
    public function destroy()
    {       
        $this->session->offsetUnset('username');
        $this->session->offsetUnset('group');       
    }
}