<?php
namespace User\Controller;
use Zend\Mvc\Controller\AbstractActionController;

use Zend\View\Model\JSonModel;
use User\Model\TbMusicPoint;
use Zend\Session\Container;
use Everyman\Neo4j\Cypher\Query;
use User\Model\Validator;

class EventController extends AbstractActionController
 {
    protected function getAuthService()
    {
        return $this->getServiceLocator()->get('AuthService');
    }
    protected function getTbUser()
    {
        return $this->getServiceLocator()->get('TbUser');
    }
    protected function getTbMusicPoint()
    {
        return $this->getServiceLocator()->get('TbMusicPoint');
    }

 }
