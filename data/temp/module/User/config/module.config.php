<?php
return ['controllers'     =>[
            'invokables'     =>[   
                    'User\Controller\Index'         => 'User\Controller\IndexController',
                    'User\Controller\Event'         => 'User\Controller\EventController',
                    'User\Controller\Point'         => 'User\Controller\PointController',
                    ]],
    'router'          =>[
            'routes'         => [
// Route cho Module User==========================================================================================           
            //dang ky-----------------------------
             'singin'    => ['type'    => 'segment',
                             'options' => [ 'route'       => '/dang-ky.html',                                          
                                            'defaults'    => ['controller' => 'User\Controller\Index',
                                                            'action'     => 'register']]],
             
             //dang nhap--------------------------
             'login'    => ['type'    => 'segment',
                             'options' => [ 'route'       => '/dang-nhap.html',                                          
                                            'defaults'    => ['controller' => 'User\Controller\Index',
                                                            'action'     => 'login']]],
             //logout--------------------------------
             'logout'    => ['type'    => 'segment',
                             'options' => [ 'route'       => '/thoat.html',                                          
                                            'defaults'    => ['controller' => 'User\Controller\Index',
                                                            'action'     => 'logout']]],
             //profile ------------------------------
             'Profile'  => ['type'     => 'segment',
                             'options' => [ 'route'       => '/trang-ca-nhan[/:username].html',
                                            'constraints' =>['username' => '[a-zA-Z0-9_-]*'],
                                            'defaults'    => ['controller' => 'User\Controller\Index',
                                                            'action'     => 'profile']]],
             //vote-----------------------
             'User_Vote' => ['type'    => 'segment',
                             'options' => [ 'route'       => '/user/vote.html',                                          
                                          'defaults'      => ['controller' => 'User\Controller\Point',
                                                            'action'     => 'vote']]],

    //===User Module===================================================================================================
    
   
    
            ], //routes
        ], //router
//end route++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     'view_manager' => [
         'template_path_stack' =>[
             'user' => __DIR__ . '/../view',
         ],
     ],
];
 ?>