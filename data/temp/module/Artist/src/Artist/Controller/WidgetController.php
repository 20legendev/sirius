<?php
namespace Artist\Controller;

use Zend\View\Model\ViewModel;
use DVGroup\Operation\BaseController;

class WidgetController extends BaseController {

	public function topProfileAction() {
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$slug = $params->fromRoute('slug', 'profile');
		$view->artist = $this->getProfile($name);
		$view->slug = $slug;
		return $view;
	}
	
	public function leftProfileAction() {
        $this->layout('layout/artist');
		$view = new ViewModel();
		$params = $this->params();
		$name = $params->fromRoute('name');
		$view->artist = $this->getProfile($name);
		return $view;
	}
	
	private function getProfile($name){
		$artist_content = $this->getTable('Artist\Model\ArtistContent');
		$artist_arr = $artist_content->getByNameSearch($name);
		if(!$artist_arr || empty($artist_arr)){
			return NULL;
		}else{
			return $artist_arr[0];
		}
	}

}
