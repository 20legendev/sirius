<?php
namespace Artist\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql; 
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use Zend\Stdlib\Hydrator\ArraySerializable;
use Zend\Db\ResultSet\HydratingResultSet;

use DVGroup\Db\Model\BaseTable;

class Artist extends BaseTable {
    		
    public function getById($id){
    	$select = $this->tableGateway->getSql()->select();
    	$select->where(array(
			'id'=>$id
		));
    	$data = $this->getCacheableData($select);
    	if($data && !empty($data)){
    		return $data[0];
    	}
    	return NULL;
    }	
    
    public function getArtist($arr){
    	$table_name = $this->tableGateway->getTable();
        $select = $this->tableGateway->getSql()->select();
        $select->columns(array('id','name','name_search','cate_id'));
        $select->join(array('ac'=>'tb_artist_content'),
        			$table_name . '.id = ac.artist_id',
        			array('ac_artist_about'=>'artist_about', 'ac_avatar'=>'avatar' ), 'left');
        $select->where(array(
        	'ac.avatar IS NOT NULL', 
        	$table_name . '.hot > 0'
        ));   
        if(isset($arr['country_id'])) {
            $select->where->and->equalTo('artist_country_id', $arr['country_id']);
        }
          
        if(isset($arr['category_id'])){
            $select->where->and->equalTo('cate_id', $arr['category_id']);
        }
        if(isset($arr['order'])){
        	$select->order($arr['order']);	
        }
        if(isset($arr['limit'])){
        	$select->limit($arr['limit']);
        	$select->offset($arr['page'] * $arr['limit']);	
        }
        
  		$result = $this->tableGateway->selectWith($select);
		$result->buffer();
		$result_array = array();
		foreach($result as $obj){
			$result_array[] = $obj;
		}
		return $result_array;
    }
    
    public function getArtistCount($arr){
    	if($arr['limit']){
    		unset($arr['limit']);
    	}
    	$result = $this->getArtist($arr);
    	return count($result);
    }
    
    public function getRelatedArtist($artist_id, $cate_id , $limit = 7, $page = 0){
    	$table_name = $this->tableGateway->getTable();
    	$select = $this -> tableGateway -> getSql() -> select();
    	$select->join(array('ac'=>'tb_artist_content'), 'ac.artist_id = ' . $table_name . '.id',
			array('ac_avatar'=>'avatar', 'name_search'));
		$select->join(array('c'=>'tb_cate'), 'c.id = ' . $table_name . '.cate_id',
			array('cat_name'=>'name', 'cat_search_name'=>'name_search'));
		$select -> where(array(
			'cate_id'=>$cate_id
		));
		$select->where->and->notEqualTo($table_name . '.id', $artist_id);
		$select -> order(new \Zend\Db\Sql\Expression('RAND()'));
		$select -> limit($limit);
		$select -> offset($page * $limit);
		return $this->getCacheableData($select);
    }
    
    
    public function getArtistDetail($name_search)
    {
        if(!$name_search)return false;
        $select=$this->tableGateway->getSql()->select();
        $select->join('tb_artist_content','id=tb_artist_content.artist_id',array('arc_birth'=>'birth',
                                                                                'arc_weight'=>'weight',
                                                                                'arc_height'=>'height',
                                                                                'arc_sex'=>'sex',
                                                                                'arc_country'=>'country',
                                                                                'arc_cung'  => 'cung',
                                                                                'arc_tuoi'  =>'tuoi',
                                                                                'arc_avatar'=>'avatar',
                                                                                'arc_avatar_goc'=>'avatar_goc',
                                                                                'arc_artist_banner_goc'=>'artist_banner_goc',
                                                                                'arc_artist_banner'=>'artist_banner',
                                                                                'arc_artist_about'=>'artist_about'));
        $select->where(array($this->tableGateway->getTable().'.name_search'=>$name_search));
        //echo $select->getSqlString();
        $rowset=$this->tableGateway->selectWith($select);
        $row=$rowset->current();
        if(!$row)
        {
            return false;
        }
        return $row;
    }
    public function getArtistDetailby_artist_id($id)
    {
        if(!$id)return false;
        $select=$this->tableGateway->getSql()->select();
        $select->join('tb_artist_content','id=tb_artist_content.artist_id',array('arc_birth'=>'birth',
                                                                                'arc_weight'=>'weight',
                                                                                'arc_height'=>'height',
                                                                                'arc_sex'=>'sex',
                                                                                'arc_country'=>'country',
                                                                                'arc_cung'  => 'cung',
                                                                                'arc_tuoi'  =>'tuoi',
                                                                                'arc_avatar'=>'avatar',
                                                                                'arc_avatar_goc'=>'avatar_goc',
                                                                                'arc_artist_banner_goc'=>'artist_banner_goc',
                                                                                'arc_artist_banner'=>'artist_banner',
                                                                                'arc_artist_about'=>'artist_about'));
        $select->where(array($this->tableGateway->getTable().'.id'=>$id));
        //echo $select->getSqlString();
        $rowset=$this->tableGateway->selectWith($select);
        $row=$rowset->current();
        if(!$row)
        {
            return false;
        }
        return $row;
    }
    
}