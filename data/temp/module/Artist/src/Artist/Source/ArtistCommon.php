<?php
namespace Artist\Source;

class ArtistCommon {
	protected static $server = 'http://video.kenhtin.net'; 
		
	public static function getUrlImageArtist($resource = '', $name_search = '', $folder = 'avatar', $cate_id = 0) {
		if ($cate_id && $cate_id > 0){
			$imageUrl = ArtistCommon::$server . '/music_file/images/' . $folder . '/' . $cate_id . '/' . str_replace(' ', '-', $name_search) . '/' . $resource;
		}else{
			$imageUrl = ArtistCommon::$server . '/music_file/images/' . $folder . '/' . str_replace(' ', '-', $name_search) . '/' . $resource;
		}	
		return $imageUrl;
	}
	
	public static function getAlbumImage($resource = '', $name_search = '') {
		return ArtistCommon::$server . '/music_file/images/album/' . str_replace(' ', '-', $name_search) . '/' . $resource;
	}

}
