<?php
namespace Artist;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Cate\Model\TbCate;
use Cate\Model\Cate;
use Artist\Model\Artist;
use Artist\Model\ArtistContent;
use Cate\Model\TbPlaylist;
use Zend\Session\Container;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module {

	public function getAutoloaderConfig() {
		return array(
		//             'Zend\Loader\ClassMapAutoloader' => array(
		//                 __DIR__ . '/autoload_classmap.php',
		//             ),
		'Zend\Loader\StandardAutoloader' => array('namespaces' => array(__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__, ), ), );
	}

	public function getConfig() {
		return
		include __DIR__ . '/config/module.config.php';
	}

	public function getServiceConfig() {
		return array('factories' => array(
		'Artist\Model\Artist' => function($sm) {
			$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			$table = new Artist(new TableGateway('tb_artist', $dbAdapter));
			return $table;
		}, 
		'Artist\Model\ArtistContent' => function($sm) {
			$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
			$table = new ArtistContent(new TableGateway('tb_artist_content', $dbAdapter));
			return $table;
		}
		
		
		));
	}

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'globwidget' => function ($sm) {
                    $serviceLocator = $sm->getServiceLocator();
                    $viewHelper = new \Home\Widget\GlobWidget();
                    $viewHelper->setServiceLocator($serviceLocator);
                    return $viewHelper;
                },
            ]
        ];
    }
}
