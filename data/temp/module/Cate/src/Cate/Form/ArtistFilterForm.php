<?php
namespace Cate\Form;

use Zend\Form\Form;
use Zend\Form\Element\Select;
use Zend\Form\Element\Submit;
use Zend\ServiceManager\ServiceManager;

class ArtistFilterForm extends Form{
	
	protected $sm;
    public function __construct($name = null, ServiceManager &$sm)
    {
        parent::__construct($name);
        
        $this->setServiceLocator($sm);
        $this->setAttribute('method', 'POST');
        
        $this->add(array(
			'name' => 'category_slug',
			'type' => 'Select',
			'options' => array(
				'label' => 'Chọn thể loại',
				'empty_option' => 'Chọn thể loại',
                'class' => 'advance-options'
			),
		));
		
        $this->add(array(
			'name' => 'country_slug',
			'type' => 'Select',
			'options' => array(
				'label' => 'Chọn quốc gia',
				'empty_option' => 'Chọn quốc gia',
                'class'=>'advance-options',
				// 'value_options' => $arr
			),
		));
		
		$this->add(array(
			'name' => 'symphony',
			'type' => 'Select',
			'options' => array(
				'label' => 'Chọn giai điệu',
				'empty_option' => 'Chọn giai điệu',
                'class'=>'advance-options',
				'value_options' => array(
					1=>'Giai điệu chưa có',
					2=>'Giai điệu chưa có 2'
				)
			),
		));
		
		$this->add(array(
			'name' => 'other_filter',
			'type' => 'Select',
			'options' => array(
				'label' => 'Chọn khác',
				'empty_option' => 'Chọn khác',
                'class'=>'advance-options',
				'value_options' => array(
					1=>'Chưa có 1',
					2=>'Chưa có 2'
				)
			),
		));
        
        $search = new Submit('search');
        $search->setValue('Tìm kiếm');
        $search->setAttributes(array(
			'value'=>'Tìm kiếm',
			'id'=>'filter_artist',
            'class'=>'btn btn-search',
		));
        
        $this->add($search);
    }
    
    public function initData($country_data, $category_data){
    	$result = array();	
    	if($country_data){
    		$arr = $country_data;
    	}else{
	    	$cate_obj = $this->getServiceLocator()->get('Cate\Model\TbCate');
	        $country_obj = $this->getServiceLocator()->get('Cate\Model\ArtistCountry');
	        
	        $category = $cate_obj->getCateMusic();
	        $arr = array();
	        foreach($category as $cate_item){
	        	$arr[$cate_item->slug] = $cate_item->name; 
	        }	
	    	$result[] = $arr;
		}
    	$this->get('category_slug')->setOptions(array(
			'value_options' => $arr
		));
    	if($category_data){
    		$arr = $category_data;
		}else{
	        $country = $country_obj->getAll();
			$arr = array();
	        foreach($country as $country_item){
	        	$arr[$country_item['country_en']] = $country_item['country_name']; 
	        }
			$result[] = $arr;
		}
		$this->get('country_slug')->setOptions(array(
			'value_options' => $arr
		));
		return $result;
    }
    
    public function setServiceLocator($sm){
    	$this->sm = $sm;
    }
    
    public function getServiceLocator(){
    	return $this->sm;
    }
    
    private function toArray($arr){
    	return \Zend\Stdlib\ArrayUtils::iteratorToArray($arr);
    }
}
