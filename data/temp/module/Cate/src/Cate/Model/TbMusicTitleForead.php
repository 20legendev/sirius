<?php
namespace Cate\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use DVGroup\Db\Model\BaseTable;

class TbMusicTitleForead extends BaseTable {
	const IS_ACTIVE = 0;
	const STATUS = 0;
	const NO_STATUS = 1;
	const TYPE_1 = 0;
	//  const TABLE_NAME = 'tb_music_title_foread_tam';// tb_music_title_foread
	const MUSIC_TYPE = 0;
	const VIDEO_TYPE = 1;
	// constant variable, song or playlist
	const SONG_TYPE = 1;
	const PLAYLIST_TYPE = 2;

	public function check() {
		$sql = new Sql($this -> tableGateway -> getAdapter());
		$select = $sql -> select('tb_music_title_foread');
		$select -> where(array('id=2'));
		$statement = $sql -> prepareStatementForSqlObject($select);
		$results = $statement -> execute();
		return $results;
	}

	public function getListMusicWithCateId($cate_id = 0, $type = 0, $sortBy = 'id') {
		if ($cate_id > 0) {
			$select = $this -> tableGateway -> getSql() -> select();
			$select -> join('tb_artist', $this -> tableGateway -> getTable() . '.artist_id=tb_artist.id', array('ar.name_search' => 'name_search'), 'left');
			$select -> where(array('tb_music_title_foread.type' => $type,
			// 'tb_music_title_foread.status'=>self::IS_ACTIVE,
			'tb_music_title_foread.cate_id' => $cate_id, 'tb_music_title_foread.file IS NOT NULL', ));

			$select -> order($sortBy . ' DESC');
			// echo $select->getSqlString();
			$kk = $this -> tableGateway -> selectWith($select);
			$resultSet = new ResultSet();
			$resultSet -> initialize($kk);
			$resultSet -> buffer();
			return $resultSet;
		} else
			return false;

	}

	public function getDetailBySlug($slug, $identify = NULL, $type = 0) {

	}

	public function getDetailByIdentify($identify = NULL) {
		if (!$identify)
			return false;

		$select = $this -> tableGateway -> getSql() -> select();

		$select -> join('tb_music_title_content_foread', $this -> tableGateway -> getTable() . '.id=tb_music_title_content_foread.id_music', array('md_content' => 'content', 'md_description' => 'description'), 'left');
		$select -> where(array('status' => self::STATUS, 'identify' => $identify));
		// echo $select->getSqlString();
		$rowset = $this -> tableGateway -> selectWith($select);
		return $rowset -> current();
	}

	public function updatePageView($id, $view) {
		return $this -> tableGateway -> update(array('view' => $view), array('id' => $id));
	}

	public function getMediaRelated($cate_id, $limit) {
		$select = $this -> tableGateway -> getSql() -> select();
		$select -> where($cate_id);
		//$select->order($order.' DESC');
		$select -> limit($limit);
		//  $select->join('tb_artist',$this->tableGateway->getTable().'.artist_id=tb_artist.id',array('name_search'));
		$select -> order(new \Zend\Db\Sql\Expression('RAND()'));
		//echo $select->getSqlString();
		$kk = $this -> tableGateway -> selectWith($select);
		$resultSet = new ResultSet();
		$resultSet -> initialize($kk);
		$resultSet -> buffer();
		return $resultSet;
	}

	public function getMediabyArtist_id($artist_id, $type = 0, $order = 'id') {
		$artist_id = (int)$artist_id;
		if (!$artist_id)
			return false;
		$select = $this -> tableGateway -> getSql() -> select();
		//$select->join('tb_artist',$this->tableGateway->getTable().'.artist_id=tb_artist.id',array('ar.name_search'=>'name_search'),'left');
		$select -> where(array('tb_music_title_foread.type' => $type, 'tb_music_title_foread.status' => self::IS_ACTIVE, 'tb_music_title_foread.artist_id' => $artist_id, 'tb_music_title_foread.file IS NOT NULL', ));

		$select -> order($order . ' DESC');
		$kk = $this -> tableGateway -> selectWith($select);
		$resultSet = new ResultSet();
		$resultSet -> initialize($kk);
		$resultSet -> buffer();
		return $resultSet;

	}
	
	public function getByArtistId($artist_id, $type = 0, $limit = 12, $page = 0, $order = 'view', $sort = 'DESC'){
		$artist_id = (int)$artist_id;
		if (!$artist_id)
			return false;
		$select = $this -> tableGateway -> getSql() -> select();
		$select -> where(array(
			'type' => $type, 
			'status' => self::IS_ACTIVE, 
			'artist_id' => $artist_id, 
			'file IS NOT NULL'
		));

		$select -> order($order . ' ' . $sort);
		$select -> limit($limit);
			$select -> offset($page * $limit);	
		return $this->getCacheableData($select);
	}
        public function getall()
    {
        return $this->TableGateway->select();
    }

	public function getFullByArtistId($artist_id, $type = 0, $limit = 12, $page = 0, $order = 'hitcount', $sort = 'DESC'){
		$artist_id = (int)$artist_id;
		if (!$artist_id)
			return false;
		$table_name = $this->tableGateway->getTable();
		$select = $this -> tableGateway -> getSql() -> select();
		$select->join(array('c'=>'tb_cate'), 'c.id = '.$table_name.'.cate_id', array('cate_name'=>'name', 'cate_search'=>'name_search'));
		$select -> where(array(
			$table_name.'.type' => $type, 
			$table_name.'.status' => self::IS_ACTIVE, 
			'artist_id' => $artist_id, 
			'file IS NOT NULL'
		));

		$select -> order($order . ' ' . $sort);
		$select -> limit($limit);
			$select -> offset($page * $limit);	
		return $this->getCacheableData($select);
	}
	
	public function countByArtistId($artist_id, $type = 0, $limit = 12){
		$artist_id = (int)$artist_id;
		if (!$artist_id)
			return false;
		$table_name = $this->tableGateway->getTable();
		$select = $this -> tableGateway -> getSql() -> select();
		$select -> where(array(
			$table_name.'.type' => $type, 
			$table_name.'.status' => self::IS_ACTIVE, 
			'artist_id' => $artist_id, 
			'file IS NOT NULL'
		));
		return ceil(count($this->getCacheableData($select))/$limit);
	}

}
