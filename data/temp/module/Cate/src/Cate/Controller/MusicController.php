<?php
namespace Cate\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DVGroup\Redis\Redis;
use Cate\Model\Cate;
use Cate\Model\CateTable;
use Cate\Model\TbMusicTitleForead;
use Zend\Paginator\Paginator;
 class MusicController extends AbstractActionController
 {
    protected $CateTable;
    protected function getCateTable()
    {
        if(!$this->CateTable)
        {
            $this->CateTable = $this->getServiceLocator()->get('Cate\Model\TbCate');
        }

        return $this->CateTable;
    }
    private function getTbMusicTitleForead()
    {
        return $this->getServiceLocator()->get('Cate\Model\TbMusicTitleForead');
    }
    //-----------------------------------------------------------------------------------
    public function musicAction()
    {
        $view=new ViewModel();
        $allCate=$this->getCateTable()->globAllCate();
        $slug=$this->params()->fromRoute('slug',0);
        $identify=$this->params()->fromRoute('identify',0);
        $redisKey='tb_music_title_foread_rd:music:identify:'.$identify;    
        // kiem tra redis
       // $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);  
       $redis=new Redis();
        $itemDetail=$redis->hgetall($redisKey);
         
        if(!$itemDetail||empty($itemDetail))
        {
                   
               $itemDetail=$this->getTbMusicTitleForead()->getDetailByIdentify($identify);
               if(!$itemDetail)
               {
                    return $this->layout('error/404');
               }
               //luu vao redis
               $itemDetail=get_object_vars($itemDetail);
               $redis->hmset($redisKey,$itemDetail);
              // echo 'db';
        }      
       //kiem tra slug
        if($itemDetail['slug']!=$slug)
        {
            return $this->redirect()->toRoute('Music',array('action'=>'music','slug'=>$itemDetail['slug'],'identify'=>$identify));
        }
        //kiem tra type
        if($itemDetail['type']==1)
        {
            return $this->redirect()->toRoute('Video',array('action'=>'video','slug'=>$itemDetail['slug'],'identify'=>$identify));
        }
        //tang visitor cho trang
        $this->getTbMusicTitleForead()->updatePageView($itemDetail['id'],$itemDetail['view']+1);
        $redis->hIncrBy($redisKey,'view',1);   

        $view->itemDetail=$itemDetail;
        $menu=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'menu')); 
        $view->addChild($menu,'menu');
        
        //so luong item cho related
        $item=10;
        //chon cache tuong ung voi related tren redis
        $rand=((int) $itemDetail['id'])%5;
        //tim cate video
        $v_cate_id=\DVGroup\Common\CommonLibs::random_cate($allCate,$itemDetail['cate_id']);        
        
        $musicrelated=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'musicrelated','cate_id'=>$itemDetail['cate_id'],'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($musicrelated,'musicrelated');
        $videorelated=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'videorelated','cate_id'=>$v_cate_id,'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($videorelated,'videorelated');
        $albumrelated=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'albumrelated','cate_id'=>$itemDetail['cate_id'],'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($albumrelated,'albumrelated');
        $music_artist_related=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'musicartistrelated','artist_id'=>$itemDetail['artist_id'],'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($music_artist_related,'music_artist_related');
        $video_artist_related=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'videoartistrelated','artist_id'=>$itemDetail['artist_id'],'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($video_artist_related,'video_artist_related');
        
        
        //var_dump($allCate);
        return $view;
    }
    
    //Video------------------------------------------------------------------------
    public function videoAction()
    {
        $view=new ViewModel();
        $allCate=$this->getCateTable()->globAllCate();
        $slug=$this->params()->fromRoute('slug',0);
        $identify=$this->params()->fromRoute('identify',0);
        $redisKey='tb_music_title_foread_rd:video:identify:'.$identify;    
        // kiem tra redis
       // $redis->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_NONE);  
       $redis=new Redis();
        $itemDetail=$redis->hgetall($redisKey);
         
        if(!$itemDetail||empty($itemDetail))
        {
                   
               $itemDetail=$this->getTbMusicTitleForead()->getDetailByIdentify($identify);
               if(!$itemDetail)
               {
                    return $this->layout('error/404');
               }
               //luu vao redis
               $itemDetail=get_object_vars($itemDetail);
               $redis->hmset($redisKey,$itemDetail);
              // echo 'db';
        }      
       //kiem tra slug
        if($itemDetail['slug']!=$slug)
        {
            return $this->redirect()->toRoute('Video',array('action'=>'video','slug'=>$itemDetail['slug'],'identify'=>$identify));
        }
        //kiem tra type
        if($itemDetail['type']==0)
        {
            return $this->redirect()->toRoute('Music',array('action'=>'detail','slug'=>$itemDetail['slug'],'identify'=>$identify));
        }
        //tang visitor cho trang
        $this->getTbMusicTitleForead()->updatePageView($itemDetail['id'],$itemDetail['view']+1);
        $redis->hIncrBy($redisKey,'view',1);    
           
        $menu=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'menu')); 
        $view->addChild($menu,'menu');
      
        $view->itemDetail=$itemDetail;
         //so luong item cho related
        $item=10;
        //chon cache tuong ung voi related tren redis
        $rand=((int) $itemDetail['id'])%5;
        //tim cate music va album
        $m_cate_id=\DVGroup\Common\CommonLibs::random_m_cate($allCate,$itemDetail['cate_id']);        
        
        $musicrelated=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'musicrelated','cate_id'=>$m_cate_id,'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($musicrelated,'musicrelated');
        $videorelated=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'videorelated','cate_id'=>$itemDetail['cate_id'],'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($videorelated,'videorelated');
        $albumrelated=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'albumrelated','cate_id'=>$m_cate_id,'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($albumrelated,'albumrelated');
        $music_artist_related=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'musicartistrelated','artist_id'=>$itemDetail['artist_id'],'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($music_artist_related,'music_artist_related');
        $video_artist_related=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'videoartistrelated','artist_id'=>$itemDetail['artist_id'],'rand'=>$rand,'limit'=>$item)); 
        $view->addChild($video_artist_related,'video_artist_related');
        

        //var_dump($allCate);
        return $view;
    }

//    private function getAdapter()
//    {
//        $sm=$this->getServiceLocator();
//        return $sm->get('Adapter');
//    }


    
}
?>