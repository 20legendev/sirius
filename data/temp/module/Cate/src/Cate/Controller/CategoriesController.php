<?php
namespace Cate\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cate\Model\Cate;
use Utf8;
use DVGroup\Redis\Redis;
use Cate\Model\CateTable;
use Cate\Model\TbMusicTitleForead;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\Iterator as paginatorIterator;

use DVGroup\Operation\BaseController;

class CategoriesController extends BaseController {
    
    public function videoAction()
    {
        $view=new ViewModel();
        $slug=$this->params()->fromRoute('slug',0);
        $sort=$this->params()->fromRoute('sort',0); 
        $page=$this->params()->fromQuery('page');
        if(!isset($page)) $page=1;
        $limit=18;      
        if(!array_key_exists($sort,$this->arrsort()))
        {
            $sort_by='id';            
        }else $sort_by=$this->arrsort()[$sort];
        $redis=new Redis();
       $cate=$this->getTable('Cate\Model\TbCate')->getCateWithSlug($slug);   
       if(!$cate) return $view;
      // $keyRedis = '_CACHE:CATE_VIDEO:' . $slug . ':LIMIT:' . $limit . ':PAGE' . $page;
       $keyRedis = '_CACHE:CATE_VIDEO:' . $slug . ':SORTBY:' . $sort_by .':LIMIT:'.$limit. ':PAGE:' . $page;
       $list_music_ =$redis->_Get($keyRedis);
       $render=array();

        if(!isset($list_music_) || empty($list_music_) || $list_music_ == '')
        {
              
          // $tbms=new TbMusicTitleForead($this->getAdapter());
           $music=$this->getTable('Cate\Model\TbMusicTitleForead')->getListMusicWithCateId($cate->id,1,$sort_by);
           $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($music);
           $paginator=new Paginator($iteratorAdapter);              
           $paginator->setCurrentPageNumber((int)$page);
           $paginator->setItemCountPerPage((int)$limit);
           $paginator->setPageRange(10);
           $total=$paginator->count() ;          
           $obj=array();
           $i=0;
           foreach($paginator as $val)
           {
                $render[]=$val;

           }
          // echo 'db';
         if(!empty($render)) $redis->_Set($keyRedis,[$render,$total],7200);
        }else
        {   
            $total=$list_music_[1];
            if(!$total){$arr=array();}else{
            $arr=array_fill(0, $total, '');}            
            $paginator=new Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($arr));              
            $paginator->setCurrentPageNumber((int)$page);
            $paginator->setItemCountPerPage(1);
            $paginator->setPageRange(10);
            $render=$list_music_[0];
            //echo 'rd';  
     
        }
       $view->paginator=$paginator;    
       $view->slug=$slug;
       $view->render=$render;
       $view->sort=$sort;
       $view->page=$page;
       
       $list = $this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'allcate','type'=>1,'route'=>'Videos','params'=>array())); 
       $menu=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'menu')); 
       $view->addChild($list,'list');
       $view->addChild($menu,'menu');  
       
       return $view;
       
    }
    public function showAction()
    {        
        $view=new ViewModel();        
        $slug=$this->params()->fromRoute('slug',0);
        $sort=$this->params()->fromRoute('sort',0);
        //echo $slug;  
        $page=$this->params()->fromQuery('page');  
        if(!isset($page)) $page=1;
        $limit=18;

        if(!array_key_exists($sort,$this->arrsort()))
        {
            $sort_by='id';            
        }else $sort_by=$this->arrsort()[$sort];
        $redis=new Redis();
       $cate=$this->getTable('Cate\Model\TbCate')->getCateWithSlug($slug);   
       if(!$cate) return $view;

      // $keyRedis = '_CACHE:CATE_VIDEO:' . $slug . ':LIMIT:' . $limit . ':PAGE' . $page;
        $keyRedis = '_CACHE:CATE_MUSIC:' . $slug . ':SORTBY:' . $sort_by .':LIMIT:'.$limit. ':PAGE:' . $page;

       $render=array();
        $list_music_ =$redis->_Get($keyRedis);
        if(!isset($list_music_) || empty($list_music_) || $list_music_ == '')
        {
              
            //$tbms=new TbMusicTitleForead($this->getAdapter());
           $music=$this->getTable('Cate\Model\TbMusicTitleForead')->getListMusicWithCateId($cate->id,0,$sort_by);   
           $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($music);
           $paginator=new Paginator($iteratorAdapter);              
           $paginator->setCurrentPageNumber((int)$page);
           $paginator->setItemCountPerPage((int)$limit);
           $paginator->setPageRange(10);
           $total=$paginator->count() ;

           $tmp=$paginator;
           foreach($tmp as $val)
           {
                $render[]=$val;
           }
          // echo 'db';
          if(!empty($render))$redis->_Set($keyRedis,[$render,$total],7200);
        }else
        {   
            $total=$list_music_[1];
            if($total==0)$total=1;
            $arr=array_fill(0, $total, '');  
            
            $paginator=new Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($arr));              
            $paginator->setCurrentPageNumber((int)$page);
            $paginator->setItemCountPerPage(1);
            $paginator->setPageRange(10);
            $render=$list_music_[0];
           // echo 'rd';  
     
        }
        $view->render=$render;
       $view->paginator=$paginator; 
      // if(!$sort)$sort='';   
       $view->slug=$slug;
       $view->sort=$sort;      
       $view->page=$page;
       
       $list = $this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'allcate','type'=>0,'route'=>'ChuyenMuc','params'=>array())); 
       $menu=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'menu')); 
       $view->addChild($list,'list');
       $view->addChild($menu,'menu');
       return $view;
    }
    
    public function artistAction() {
        $this->layout('layout/artist');
        $view = new ViewModel();
        $cate_obj = $this->getTable('Cate\Model\TbCate');
        $country_obj = $this->getTable('Cate\Model\ArtistCountry');
        $artist_obj = $this->getTable('Artist\Model\Artist');
         
		$request = $this->getRequest();
		$params = $this->params();
		if($request->isPost()){
			$country_slug = $this->params()->fromPost('country_slug');
			$category_slug = $this->params()->fromPost('category_slug');
			$arr = array();
			if($country_slug) $arr['country_slug'] = $country_slug;
			if($category_slug) $arr['category_slug'] = $category_slug; 
			return $this->redirect()->toRoute('Artist', $arr);
		}else{
			$category_slug = $params->fromRoute('category_slug');
        	$country_slug = $params->fromRoute('country_slug');
		} 
        $page = $this->getRealPage($params->fromRoute('page', 1));
        
        $limit = $params->fromQuery('perpage', 12);
        $redis=new Redis();
        $key = '_CACHE:ARTIST:' . $country_slug . ':CATE:'.$category_slug.'SORTBY:hot:LIMIT:'.$limit. ':PAGE:' . $page;
        $cached_data = $redis->_Get($key);
        $enable_cache = false;
        if(1 || !$enable_cache || !$cached_data || empty($cached_data)){
        	
        	$cate_data = $cate_obj->getBySlug($category_slug);
			$country_data = $country_obj->getBySlug($country_slug);
			$cate_id = $cate_data ? $cate_data->id : NULL;
			$country_id = $country_data ? $country_data->artist_country_id : NULL;	
			$array = array(
				'category_id' => $cate_id,
				'country_id' => $country_id,
				'page'=>$page,
				'limit'=>$limit,
				'order'=>'hot DESC'
			);
			$view->artist = $artist_obj->getArtist($array);
			$view->total_page = ceil($artist_obj->getArtistCount($array)/$limit);
			$view->countries = $country_obj->getAll();
			
			$config = $this->getServiceLocator()->get('config');
			$config = $config['CACHE_TIME'];
			$redis->_Set($key, array($view->countries, $view->artist, $view->total_page), $config['SHORT']);
		}else{
			$view->countries = $cached_data[0];
			$view->artist = $cached_data[1];
			$view->total_page = intval($cached_data[2]);
		}
		$artist_filter = $this->forward()->dispatch('Cate\Controller\Widget', array(
			'action' => 'artist-filter',
			'category_slug'=>$category_slug,
			'country_slug'=>$country_slug
		));
		$view->addChild($artist_filter, 'artist_filter');
        $view->country_slug = $country_slug;
        $view->category_slug = $category_slug;
        
        $view->is_next = $page + 1 < $view->total_page ? $page + 1 : -1;
        $view->is_back = $page > 0 ? $page : -1; 
		return $view;
    }
    
    public function arrsort() {
        return array('new'=>'id','hot'=>'view','like'=>'like');
    }
    
    private function getRealPage($page){
    	return $page - 1;
    }





    
}
?>
