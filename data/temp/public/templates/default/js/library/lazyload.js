LazyLoad = {
    init: function(){
        var obj = $('.lazy-image');
        var src = '';
        for(var i=0; i < obj.length; i++){
            src = $(obj[i]).attr('lazy-src');
            (function(jObject, imageSrc){
                var img = new Image();
                img.onload = function(){
                    jObject.css('background-image', 'url('+imageSrc+')');
                }
                img.src = imageSrc;
            })($(obj[i]), src)
        }
    }
}