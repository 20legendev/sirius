<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
require 'define.php';
chdir(dirname(__DIR__));
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}
//define('SOLR_SERVER_HOSTNAME', '172.16.0.13');

/* HTTP Port to connection */
//define('SOLR_SERVER_PORT',  8983);
// Setup autoloading
require 'init_autoloader.php';
require 'vendor/FirePHPCore/FirePHP.class.php';
require 'vendor/FirePHPCore/fb.php';
// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();
