<?php 
namespace DVGroup\SVN;

class SVNLib{
	public static function checkoutSvn($path, $local_path, $username, $password){
		svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_USERNAME, $username);
		svn_auth_set_parameter(SVN_AUTH_PARAM_DEFAULT_PASSWORD, $password);
		svn_auth_set_parameter(PHP_SVN_AUTH_PARAM_IGNORE_SSL_VERIFY_ERRORS, true);
		
		echo "checking out...".$path."<br/>";
		if(svn_checkout($path, $local_path)){
			echo "check out done...<br/>";
		}else{
			echo "check out error...<br/>";
			return false;
		}
		return true;
	}
}
?>