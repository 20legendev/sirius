<?php

namespace DVGroup\SVN;

class FTPLib {
	public static function uploadToServer($server, $username, $password) {
		$conn_id = ftp_connect ( $server ) or die ( "<p style=\"color:red\">Error connecting to $server </p>" );
		echo "init deploy environment...<br>";
		if (ftp_login ( $conn_id, $username, $password )) {
			ftp_pasv ( $conn_id, true );
			echo "start deployment...<br>";
			FTPLib::putAll ( $conn_id, getcwd () . '/data/temp', '/public_html/demo/test' );
			echo "Deployment completed successfully<br>";
		} else {
			echo "init deploy environment error...<br>";
		}
	}
	private static function putAll($conn_id, $src_dir, $dst_dir) {
		$directory = dir ( $src_dir );
		while ( $file = $directory->read () ) {
			if ($file != "." && $file != "..") {
				if ($file != '.svn') {
					if (is_dir ( $src_dir . "/" . $file )) {
						if (! @ftp_chdir ( $conn_id, $dst_dir . "/" . $file )) {
							ftp_mkdir ( $conn_id, $dst_dir . "/" . $file );
						}
						FTPLib::putAll ( $conn_id, $src_dir . "/" . $file, $dst_dir . "/" . $file );
					} else {
						$upload = ftp_put ( $conn_id, $dst_dir . "/" . $file, $src_dir . "/" . $file, FTP_BINARY );
					}
				}
			}
		}
		$directory->close ();
	}
}
?>