<?php
return array (
		'controllers' => [ 
				'invokables' => [ 
						'Project\Controller\Index' => 'Project\Controller\IndexController',
						'Project\Controller\Widget' => 'Project\Controller\WidgetController' 
				] 
		],
		'router' => [ 
				'routes' => [ 
						'home' => array (
								'type' => 'Zend\Mvc\Router\Http\Literal',
								'options' => array (
										'route' => '/',
										'defaults' => array (
												'controller' => 'Project\Controller\Index',
												'action' => 'index' 
										) 
								),
								'may_terminate' => true,
								'child_routes' => array (
										'default' => array (
												'type' => 'Segment',
												'options' => array (
														'route' => '[:controller[/:action]]',
														'constraints' => array (
																'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
																'action' => '[a-zA-Z][a-zA-Z0-9_-]*' 
														),
														'defaults' => array (
																'action' => 'index',
																'__NAMESPACE__' => 'Project\Controller' 
														) 
												) 
										) 
								) 
						),
						'project_detail' => array (
								'type' => 'segment',
								'options' => array (
										'route' => '/project-detail[/:project_id]',
										'constraints' => array (
												'project_id' => '[0-9]*' 
										),
										'defaults' => array (
												'controller' => 'Project\Controller\Index',
												'action' => 'detail' 
										) 
								) 
						) 
				] 
		],
		'view_manager' => [ 
				'template_path_stack' => [ 
						'album' => __DIR__ . '/../view' 
				],
				'template_map' => [ 
						'layout/template' => __DIR__ . '/../view/layout/layout.phtml' 
				] 
		] 
);
?>