<?php

namespace Project\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use DVGroup\Db\Model\BaseTable;

class Project extends BaseTable {
	public function getAllProject() {
		$select = $this->tableGateway->getSql ()->select ();
		$select->where ( array (
				'is_active' => 1 
		) );
		$select->order ( 'project_name ASC' );
		$result = $this->tableGateway->selectWith ( $select );
		$result->buffer ();
		return $result;
	}
	
	public function getProjectById($project_id) {
		$select = $this->tableGateway->getSql ()->select ();
		$select->where ( array (
				'project_id' => $project_id
		) );
		$result = $this->tableGateway->selectWith ( $select );
		$row = $result->current();
		if($row) return $row;
		return NULL;;
	}
}
