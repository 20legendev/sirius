<?php

namespace Project\Controller;

use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
Use DVGroup\Operation\BaseController;
use DVGroup\SVN\SVNLib;
use DVGroup\SVN\FTPLib;

class WidgetController extends BaseController {
	
	public function leftMenuAction() {
		$view = new ViewModel();
		$project_id = $this->params()->fromRoute('project_id');
		$project = $this->getTable('Project\Model\Project');
		$view->project = $project->getAllProject();
		if($project_id){
			$view->project_id = $project_id;
		}
		return $view;
	}
}