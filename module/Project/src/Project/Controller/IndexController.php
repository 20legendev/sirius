<?php

namespace Project\Controller;

use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
Use DVGroup\Operation\BaseController;
use DVGroup\SVN\SVNLib;
use DVGroup\SVN\FTPLib;

class IndexController extends BaseController {
	
	public function indexAction() {
		$view = new ViewModel();
		$project = $this->getTable('Project\Model\Project');
		$view->project = $project->getAllProject();
		return $view;
	}
	
	public function detailAction() {
		$view = new ViewModel();
		$project_id = $this->params()->fromRoute('project_id');
		$this->prepare($project_id, $view);
		
		$project = $this->getTable('Project\Model\Project');
		$view->project = $project->getProjectById($project_id);
		return $view;
	}
	
	private function prepare($project_id = NULL, &$view){
		$left_menu = $this->forward()->dispatch('Project\Controller\Widget', array(
				'project_id'=>$project_id,
				'action'=>'left-menu'
		));
		$view->addChild($left_menu, 'left_menu');
	}
}